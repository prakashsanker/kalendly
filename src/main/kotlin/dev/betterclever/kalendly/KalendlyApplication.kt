package dev.betterclever.kalendly

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KalendlyApplication

fun main(args: Array<String>) {
	runApplication<KalendlyApplication>(*args)
}
