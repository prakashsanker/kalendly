package dev.betterclever.kalendly.repositories

import dev.betterclever.kalendly.controllers.AMCreateUser
import dev.betterclever.kalendly.controllers.AMWeekdayAvailability
import dev.betterclever.kalendly.db.Tables
import dev.betterclever.kalendly.db.tables.Users
import dev.betterclever.kalendly.db.tables.records.WeekdayAvailabilityRecord
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.springframework.stereotype.Repository
import java.sql.Connection
import java.time.DayOfWeek

@Repository
class UserRepository {

    fun create(user: AMCreateUser, conn: Connection): Int {
        val context = DSL.using(conn, SQLDialect.POSTGRES)
        val userId =  context.insertInto(Tables.USERS, Tables.USERS.USERNAME)
            .values(user.username)
            .returningResult(Users.USERS.ID)
            .fetch()[0].value1()

        return userId
    }

    fun updateAvailability(userId: Int, availabilityByDayOfWeek: Map<DayOfWeek, AMWeekdayAvailability>, conn: Connection) {
        val context = DSL.using(conn, SQLDialect.POSTGRES)

        val currentAvailabilityByDayOfWeek = getAvailability(userId, conn)
        val queries = availabilityByDayOfWeek.map { (dayOfWeek, updatedAvailability) ->
            val currentAvailability = currentAvailabilityByDayOfWeek[dayOfWeek]
            if (currentAvailability != null) {
                context.update(Tables.WEEKDAY_AVAILABILITY)
                    .set(Tables.WEEKDAY_AVAILABILITY.START_TIME, updatedAvailability.startTime)
                    .set(Tables.WEEKDAY_AVAILABILITY.END_TIME, updatedAvailability.endTime)
                    .where(Tables.WEEKDAY_AVAILABILITY.USER_ID.eq(userId))
                    .and(Tables.WEEKDAY_AVAILABILITY.DAY_OF_WEEK.eq(dev.betterclever.kalendly.db.enums.DayOfWeek.valueOf(dayOfWeek.name)))
            } else {
                context.insertInto(Tables.WEEKDAY_AVAILABILITY)
                    .columns(Tables.WEEKDAY_AVAILABILITY.DAY_OF_WEEK, Tables.WEEKDAY_AVAILABILITY.USER_ID, Tables.WEEKDAY_AVAILABILITY.START_TIME, Tables.WEEKDAY_AVAILABILITY.END_TIME)
                    .values(dev.betterclever.kalendly.db.enums.DayOfWeek.valueOf(updatedAvailability.dayOfWeek.name), userId, updatedAvailability.startTime, updatedAvailability.endTime)
            }
        }

        context.batch(queries).execute()
    }

    fun getAvailability(userId: Int, conn: Connection): Map<DayOfWeek, WeekdayAvailabilityRecord> {
        val context = DSL.using(conn, SQLDialect.POSTGRES)

        return context.selectFrom(Tables.WEEKDAY_AVAILABILITY)
            .where(Tables.WEEKDAY_AVAILABILITY.USER_ID.eq(userId))
            .fetch()
            .associateBy { it.dayOfWeek }
            .mapKeys { DayOfWeek.valueOf(it.key.name) }
    }
}